CREATE TABLE IF NOT EXISTS polls (
	id INTEGER NOT NULL PRIMARY KEY, 
	guid TEXT, 
	title TEXT,
	description TEXT,
	choices TEXT,
	created_at TEXT,
	updated_at TEXT
);

CREATE TABLE IF NOT EXISTS votes (
	id INTEGER NOT NULL PRIMARY KEY, 
	poll_id INTEGER, 
	vote TEXT,
	created_at TEXT,
	FOREIGN KEY(poll_id) REFERENCES polls(id)
);
