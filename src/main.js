// 1. get db connected
// 2. setup express web server
const express = require('express');
const app = express();
const port = 8080;
const db = require('./helpers/db');
const cors = require('cors');
const bodyParser = require('body-parser');
const polls = require('./models/polls');

db.initDB();
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'));

// Healthy endpoint check
app.get('/healthy', (req, res) => {
  res.send(`Current date is ${new Date}`);
});

// Posting/creating a poll
app.post('/polls', (req, res) => {
  const returnData = polls.newPoll(req.body);
  res.send(returnData);
});

// Get a poll by it's guid
app.get('/polls/:guid', (req, res) => {
  res.send(polls.getPoll(req.params.guid));
});

// Vote on poll
app.post('/polls/:guid/vote', (req, res) => {
  console.log(req.params.guid, req.body);
  res.send(201);
});
