Want:
* express
* body-parser
* express-static
* better-sqlite3
* cors


write all commands in .sql file

## API contract for submitting a new Poll

### FE sends:
* name of poll
* description
* array of choice objects: Array<string>
```json
{
  "title": "This is a poll title",
  "description": "This is a description field for a ranked choice poll",
  "choices": [
    {
      "id": "1234",
      "title": "Choice 1"
    }, {
      "id": "1235",
      "title": "Choice 2"
    }, {
      "id": "1236",
      "title": "Choice 3"
    }, {
      "id": "1237",
      "title": "Choice 4"
    }
  ]
}
```

### BE will 
* convert that array for the table
* generate the GUID and send back FE 
* also adds date creating stamp for db 

send in response:
```json
{
  "id": 3,
  "guid": "cb408cf0-d4f6-11ea-9eef-7b2c4e1fba7a",
  "created_at": "string_date",
  "updated_at": "string_date",
  "title": "This is a poll title",
  "description": "This is a description field for a ranked choice poll",
  "choices": [
    {
      "id": "1234",
      "title": "Choice 1"
    }, {
      "id": "1235",
      "title": "Choice 2"
    }, {
      "id": "1236",
      "title": "Choice 3"
    }, {
      "id": "1237",
      "title": "Choice 4"
    }
  ]
}
```

## API contract for requesting an existing Poll
FE sends guid
```json
{
  "guid": "cb408cf0-d4f6-11ea-9eef-7b2c4e1fba7a"
}
```

BE sends poll (later it will join with the votes)
```json
  "id": 3,
  "guid": "cb408cf0-d4f6-11ea-9eef-7b2c4e1fba7a",
  "created_at": "string_date",
  "updated_at": "string_date",
  "title": "This is a poll title",
  "description": "This is a description field for a ranked choice poll",
  "choices": [
    {
      "id": "1234",
      "title": "Choice 1"
    }, {
      "id": "08645",
      "title": "Choice 2"
    }, {
      "id": "76745",
      "title": "Choice 3"
    }, {
      "id": "997612",
      "title": "Choice 4"
    }
  ]
}
```
