const sqlite = require('better-sqlite3');
const fs = require('fs');
const path = require('path');
let db;

/* Called when program starts in main.js.
 * It will create the intial connection to the database
 */
const initDB = () => {
  db = new sqlite(path.join(__dirname, '../../db/rank-choice.sqlite3'));
  const initPath = path.join(__dirname, '../../migrations/init.sql');
  const migration = fs.readFileSync(initPath, 'utf8');
  db.exec(migration);
};

/* Returns a live connectin to the database.
 * If it can't find one it will create a new connection and return that.
 */
const getDBCon = () => {
  return db;
};

/* Inserts new poll into the database.
 * Calls getDBCon to get a functional db connection
 * @param pollEntry
 */
const insertNewPoll = (pollEntry) => {
  const dbCon = getDBCon();
  const stmt = dbCon.prepare('INSERT INTO polls (guid, title, description, choices, created_at, updated_at) VALUES (@guid, @title, @description, @choices, @created_at, @updated_at);');

  const runStmt = stmt.run({
    guid: pollEntry.guid,
    title: pollEntry.title,
    description: pollEntry.description,
    choices: JSON.stringify(pollEntry.choices),
    created_at: pollEntry.created_at,
    updated_at: pollEntry.updated_at,
  });

  pollEntry['id'] = runStmt.lastInsertRowid;

  return pollEntry;
};

/* Inserts new poll into the database.
 * Gets poll's id and writes new vote (stringified) to the table
 * @param pollGuid
 */
const selectPoll = (pollGuid) => {
  const dbCon = getDBCon();
  const stmt = dbCon.prepare('SELECT * FROM polls WHERE guid = ?');
  const poll = stmt.get(pollGuid);

  return poll;
}

/* Gets poll id based off guid.
 * @param pollGuid
 */
const getPollId = (pollGuid) => {
  const dbCon = getDBCon();
  const stmt1 = dbCon.prepare('SELECT id FROM polls WHERE guid = ?');
  const poll_id = stmt1.get(pollGuid);

  return poll_id;
}

/* gets a poll (by GUID).
 * returns poll data model
 * @param pollGuid
 */
const insertNewVote = (voteData) => {
  const dbCon = getDBCon();
  const stmt = dbCon.prepare('INSERT INTO votes (poll_id, vote, created_at) VALUES (@poll_id, @vote, @created_at);');

  const runStmt = stmt.run({
    poll_id: voteData.poll_id,
    vote: JSON.stringify(voteData.vote),
    created_at: voteData.created_at,
  });

  return runStmt;
}

module.exports = { insertNewPoll, selectPoll, getPollId, insertNewVote, initDB };