const uuid = require('uuid');

const generateGuid = () => {
  return uuid.v1();
}

module.exports = { generateGuid };