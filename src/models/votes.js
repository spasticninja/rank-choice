const db = require('../helpers/db');

const newVote = (pollGuid, voteData) => {
  const date = new Date();
  const poll_id = db.getPollId(pollGuid);

  voteData['poll_id'] = poll_id;
  voteData['created_at'] = date.toString();

  return db.insertNewVote(voteData);
};

module.exports = { newVote };