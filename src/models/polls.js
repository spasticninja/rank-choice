const { generateGuid } = require('../helpers/pollData');
const db = require('../helpers/db');

const newPoll = (pollData) => {
  const date = new Date();
  
  let pollObject = pollData;
  pollObject['created_at'] = date.toString();
  pollObject['updated_at'] = date.toString();
  pollObject['guid'] = generateGuid();

  return db.insertNewPoll(pollObject);
};

const getPoll = (pollGuid) => {
  const returnPoll = db.selectPoll(pollGuid);

  returnPoll.choices = JSON.parse(returnPoll.choices);

  return returnPoll;
}

module.exports = { newPoll, getPoll };