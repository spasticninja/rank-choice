const polls = {};

export function storePoll(guid, poll) {
  polls[guid] = poll;
}

export function getPoll(guid) {
  return polls[guid];
}
