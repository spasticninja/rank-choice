'use strict';

import m from 'mithril';

import CreatePoll from './views/CreatePoll.jsx';
import Landing from './views/Landing.jsx';
import PollResults from './views/PollResults.jsx';
import VotePoll from './views/VotePoll.jsx';
import View404 from './views/404.jsx';

const root = document.getElementById('root');

m.route(root, '/', {
  '/': Landing,
  '/404': View404,
  '/create': CreatePoll,
  '/polls/:key/vote': VotePoll,
  '/polls/:key/view': PollResults,
});
