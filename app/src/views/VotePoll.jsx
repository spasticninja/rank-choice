/* @jsx m */
import m from 'mithril';
import * as pollCache from '../helpers/pollCache.js';
import Sortable, {Swap} from 'sortablejs';

Sortable.mount(new Swap());

export default class VotePoll {
  constructor(vnode) {
    const guid = vnode.attrs.key;
    if(!guid) console.log('Bad route');
    this.title = "...loading";
    this.description = "";
    this.choices = [];
    this.disableSubmit = true;
    this.renderPoll(guid);
  }

  async renderPoll(guid) {
    return this.getPoll(guid).then(poll => {
      this.title = poll.title;
      this.description = poll.description;
      this.choices = poll.choices;
      this.guid = poll.guid;
    });
  }

  getPoll(guid) {
    //try cache
    let poll = pollCache.getPoll(guid);
    if(poll && poll.guid) {
      return poll
    }

    return m.request({
      method: 'GET',
      url: 'http://localhost:8080/polls/' + guid
    }).then((res) => {
      if(!res || !res.guid) {
        m.route.set('/404')
      }
      pollCache.storePoll(res.guid, res);
      this.disableSubmit = false;
      return res
    }).catch((e) => {
      console.log(`Issue retrieving poll ${e}`);
    })
  }

  submitForm() {
    this.disableSubmit = true;
    const sortables = document.getElementById("sortWrapper").children;
    const vote = [];
    for( const choice of sortables ) {
      vote.push(choice.dataset.id);
    }
    
    return m.request({
      method: 'POST',
      url: `http://localhost:8080/polls/${this.guid}/vote`
    }).then((res) => {
      m.route.set(`/polls/${this.guid}/view`);
      // if(!res || !res.guid) {
      //   m.route.set('/404')
      // }
      // pollCache.storePoll(res.guid, res);
    }).catch((e) => {
      console.log(`Issue voting on poll ${e}`);
    }).finally(() => {
      this.disableSubmit = false;
    });
  }

  view() {
    return (
      <div className="column">
        <h2>Vote on {this.title}</h2>
        <p>{this.description}</p>
        <p>Please vote by dragging choices into your desired order and then submit</p>
        <div id="sortWrapper" class="sortable">
          {
            this.choices.map( (choice, index) => {
            return (
              <p class="sortChild" data-id={choice.id}><span class="bx bx-dots-vertical-rounded"></span>{choice.title}</p>
            )
            } )
          }
        </div>
        <button class="button-primary float-left" disabled={this.disableSubmit} onclick={this.submitForm.bind(this)}>Submit</button>
      </div>
    )
  }

  oncreate(vnode) {
    const el = document.querySelector('#sortWrapper');
    if(!el) return;

    this.sortable = new Sortable(el, {
      swap: true, // Enable swap mode
      swapClass: "sortChild-drop" // Class name for swap item (if swap mode is enabled)
    });
  }
}