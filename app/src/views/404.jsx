/* @jsx m */
import m from 'mithril';

export default function Landing() {
  return {
    view: () => (<div className="column">
        <h2>404</h2>
        <p>Poll not found</p>
        <a href="/#/create" className="button">Create A Poll</a>
      </div>)
  };
}