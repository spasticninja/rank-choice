/* @jsx m */
import m from 'mithril';
import { powerform, Field } from "powerform";
import * as pollCache from '../helpers/pollCache.js';

export default class CreatePoll {
    constructor() {
      this.choices = [''];
      this.formSchema = {
        title: [],
        description: []
      };

      this.powerform = powerform(this.formSchema);
      this.disableSubmit = false;
      this.showNotification = false;
      this.notificationText = '';
    }

    updateChoice(index, e) {
      const fieldName = "choice"+index;
      if (!this.powerform[fieldName]) {
        this.addChoice(index);
      }
      this.powerform[fieldName].setData(e.target.value);
      this.choices[index] = e.target.value;
    }

    addChoice(index) {
      const fieldName = "choice"+index;
      this.powerform[fieldName] = new Field({});
      this.powerform.fieldNames.push(fieldName);
      this.choices.push('');
    }

    resetForm() {
      this.choices = [''];
      this.powerform.reset();
    }

    getSubmissionBody() {
      const choices = [];

      this.choices.forEach( (choice, index) => {
        if(!choice.length) return;

        choices.push({
          id: "choice_" + index,
          title: choice
        });
      });

      return  {
        title: this.powerform.title.getData(),
        description: this.powerform.description.getData(),
        choices
      }
    }

    submitForm(e) {
      e.preventDefault();
      if (this.disableSubmit) return;

      this.disableSubmit = true;
      this.notificationText = 'Creating Poll...';
      this.showNotification = true;

      const body = this.getSubmissionBody();

      m.request({
        method: 'POST',
        url: 'http://localhost:8080/polls',
        body
      }).then((res) => {
        this.notificationText = "Poll created!";
        pollCache.storePoll(res.guid, res);
        this.resetForm();
        m.route.set(`/polls/${res.guid}/view`);
      }).catch((e) => {
        this.notificationText = `Error creating poll: ${e}`;
      }).finally(() => {
        this.disableSubmit = false;
      });
    }

    view() {
      return (
      <div className="column">
        <h2>Create New Poll</h2>
        <div class={ this.showNotification ? '' : 'hide'}>
          <p>{this.notificationText}</p>
        </div>
        <form action="">
          <fieldset>
            <label HtmlFor="titleField">Title</label>
            <input type="text" placeholder="Where are we going for lunch?" id="titleField" oninput={e => this.powerform.title.setData(e.target.value)} onchange={() => this.powerform.title.validate()}/>
            <label HtmlFor="descriptionField">Description</label>
            <textarea placeholder="Please don't say pizza again." id="descriptionField" oninput={e => this.powerform.description.setData(e.target.value)} onchange={() => this.powerform.description.validate()}></textarea>
            {
              this.choices.map( (choice, index) => {
              return (
                <span>
                  <label HtmlFor={"choice"+index}>Choice {index + 1}</label>
                  <input type="text" placeholder={"Choice " + (index + 1)} id={"choice"+index} oninput={this.updateChoice.bind(this, index)}/>
                </span>
              )
              } )
            }
            <button class="button-primary float-left" disabled={this.disableSubmit} onclick={this.submitForm.bind(this)}>Submit</button>
            <button class="button button-clear float-right" onclick={this.resetForm.bind(this)}>Reset</button>
          </fieldset>
        </form>
      </div>
    )
  }
}