/* @jsx m */
import m from 'mithril';

export default function Landing() {
  return {
    view: () => (<div className="column">
        <p>Rank Choice voting is a method of voting in which each participant ranks their choices. Instead of a winner take all approach where a simple majority always wins, rank choice voting allows consensus based choices to win.</p>
        <p>This app is a quick tool for building rank choice based polls and letting folks vote on them anonymously and easily. There's no authentication needed and whether you're deciding what to get for lunch or who to make president of the fan club, this is the app for you.</p>
        <p>Note that we only keep your polls for about 24 hours. This is both to protect your privacy and to keep costs low.</p>

        <a href="/#/create" className="button">Create A Poll</a>
      </div>)
  };
}