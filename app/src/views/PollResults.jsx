/* @jsx m */
import m from 'mithril';
import * as pollCache from '../helpers/pollCache.js';

export default class PollResults {
  constructor(vnode) {
    const guid = vnode.attrs.key;
    if(!guid) console.log('Bad route');
    this.title = "...loading";
    this.description = "";
    this.choices = [];
    this.renderPoll(guid);
  }

  async renderPoll(guid) {
    return this.getPoll(guid).then(poll => {
      this.title = poll.title;
      this.description = poll.description;
      this.choices = poll.choices;
    });
  }

  getPoll(guid) {
    //try cache
    let poll = pollCache.getPoll(guid);
    if(poll && poll.guid) {
      return poll
    }

    return m.request({
      method: 'GET',
      url: 'http://localhost:8080/polls/' + guid
    }).then((res) => {
      if(!res || !res.guid) {
        m.route.set('/404')
      }
      pollCache.storePoll(res.guid, res);
      return res
    }).catch((e) => {
      console.log(`Issue retrieving poll ${e}`);
    })
  }

  view() {
    return (
      <div className="column">
        <h2>Viewing Poll: {this.title}</h2>
        <p>{this.description}</p>
        {
          this.choices.map( (choice, index) => {
          return (
            <span>
              <p>{choice.title}</p>
            </span>
          )
          } )
        }

      </div>
    )
  }
}